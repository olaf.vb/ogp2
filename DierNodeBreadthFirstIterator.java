package diersoorten;

import java.util.Stack;

public class DierNodeBreadthFirstIterator implements DierNodeIterator
{
    private Stack<DierNode> fringe = new Stack<DierNode>();

    public DierNodeBreadthFirstIterator(DierNode headNode)
    {
        fringe.push(headNode);
    }

    public DierNode getNext()
    {
        
        if(!hasMore())
            return null;
        
        DierNode node = fringe.pop();
        
        int i = 0;
        
        Stack<DierNode> tempFringe = new Stack<DierNode>();

        while(node.GetChild(i) != null)
        {
            tempFringe.push(node.GetChild(i));    
            i++;
        }
        while(hasMore())
        {
            tempFringe.push(fringe.pop());
        }
        fringe = tempFringe;


        return node;
    }

    public boolean hasMore()
    {
        return !fringe.empty();
    }



}