package diersoorten;

import java.util.Stack;

public class DierNodeDepthFirstIterator implements DierNodeIterator
{
    private Stack<DierNode> fringe = new Stack<DierNode>();

    public DierNodeDepthFirstIterator(DierNode headNode)
    {
        fringe.push(headNode);
    }

    public DierNode getNext()
    {
        
        if(!hasMore())
            return null;
        
        DierNode node = fringe.pop();
        
        int i = 0;
        while(node.GetChild(i) != null)
        {
            fringe.push(node.GetChild(i));
            i++;
        }
        
        return node;
    }

    public boolean hasMore()
    {
        return !fringe.empty();
    }



}